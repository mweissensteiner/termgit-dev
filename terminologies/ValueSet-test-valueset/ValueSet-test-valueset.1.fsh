Instance: test-valueset 
InstanceOf: ValueSet 
Usage: #definition 
* id = "test-valueset" 
* url = "https://mweissensteiner.gitlab.io/ValuteSet/test-valueset" 
* name = "test-valueset" 
* title = "Test eines Value Sets" 
* status = #draft 
* version = "1.0.0" 
* description = "**Test ValueSet**" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:11.22.33.44.55" 
* date = "2023-03-14" 
* contact[0].name = "Mein Kontakt" 
* contact[0].telecom[0].system = #url 
* contact[0].telecom[0].value = "www.landesgesundheitsagentur.at/" 
* compose.include[0].system = "https://termgit.elga.gv.at/CodeSystem/appc-anatomie"
* compose.include[0].version = "1.0.0+20230131"
* compose.include[0].concept[0].code = #0
* compose.include[0].concept[0].display = "Maßnahme ohne regionale Zuordnung"
* compose.include[0].concept[0].designation[0].language = #de-AT 
* compose.include[0].concept[0].designation[0].value = "coole Maßnahme" 
* compose.include[0].concept[1].code = #1
* compose.include[0].concept[1].display = "Maßnahme2 ohne regionale Zuordnung"
* compose.include[0].concept[1].designation[0].language = #de-DE 
* compose.include[0].concept[1].designation[0].value = "Tolle Maßnahme" 
* compose.include[0].concept[2].code = #2
* compose.include[0].concept[2].display = "Maßnahme3 ohne regionale Zuordnung"
* compose.include[0].concept[2].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* compose.include[0].concept[2].designation[0].value = "Tolle Coole Maßnahme zur Selbstanwendung" 

* expansion.timestamp = "2023-03-14T15:48:53.0000Z"

* expansion.contains[0].system = "https://termgit.elga.gv.at/CodeSystem/appc-anatomie"
* expansion.contains[0].version = "1.0.0+20230131"
* expansion.contains[0].code = #0
* expansion.contains[0].display = "Maßnahme ohne regionale Zuordnung"
* expansion.contains[0].designation[0].language = #de-AT 
* expansion.contains[0].designation[0].value = "coole Maßnahme" 
* expansion.contains[0].contains[0].system = "https://termgit.elga.gv.at/CodeSystem/appc-anatomie"
* expansion.contains[0].contains[0].version = "1.0.0+20230131"
* expansion.contains[0].contains[0].code = #1
* expansion.contains[0].contains[0].display = "Maßnahme2 ohne regionale Zuordnung"
* expansion.contains[0].contains[0].designation[0].language = #de-DE 
* expansion.contains[0].contains[0].designation[0].value = "Tolle Maßnahme" 
* expansion.contains[0].contains[1].system = "https://termgit.elga.gv.at/CodeSystem/appc-anatomie"
* expansion.contains[0].contains[1].version = "1.0.0+20230131"
* expansion.contains[0].contains[1].code = #2
* expansion.contains[0].contains[1].display = "Maßnahme3 ohne regionale Zuordnung"
* expansion.contains[0].contains[1].designation[0].use = https://termgit.elga.gv.at/CodeSystem/austrian-designation-use#hinweise "hinweise" 
* expansion.contains[0].contains[1].designation[0].value = "Tolle Coole Maßnahme zur Selbstanwendung" 
